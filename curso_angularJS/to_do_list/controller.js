angular.module("ToDoList",["LocalStorageModule"])
.controller("ToDoController", function ($scope,localStorageService) {
  if(localStorageService.get("angularList")){
    $scope.todo = localStorageService.get("angularList");
  }else {
    $scope.todo = [];
  }

$scope.$watchCollection('todo',function() {
  return $scope.newActv;
},function (newValue,oldValue) {
  localStorageService.set("angularList",$scope.todo);

});

  $scope.addActividad = function() {
      $scope.todo.push($scope.newActv);
      $scope.newActv = {};

  }
  $scope.clean = function(){
    $scope.todo = [];
  }
})
