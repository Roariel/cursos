class Cliente(Persona):
    """docstring for Cliente."""
    def __init__(self, p_apellido,p_nombre,p_bici,p_id):
        Persona.__init__(self, p_apellido,p_nombre)
        self.bicletaDescripcion = p_bici.bicletaDescripcion
        self.id = p_id
        self.billetera = []

    def get_descripcion_bici(self):
        return self.bicletaDescripcion

    def guardar_en_billetera(self, p_item):
        self.get_billetera().append(p_item)
