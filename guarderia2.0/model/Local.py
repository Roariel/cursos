class Local(object):
    """docstring forLocal."""

    def __init__(self, p_nombre, p_direccion, p_operador):
        self.nombre = p_nombre
        self.direccion = p_direccion
        self.tk = Tikeadora()
        self.operador = p_operador
        self.espacios = []
        self.clientes = {}

    def get_nombre(self):   return self.nombre;
    def get_tikeadota(self):   return self.tk;
    def get_espacio(self):   return self.espacios;
    def get_clientes(self):   return self.clientes;
