class Persona(object):

    def __init__(self, p_apellido,p_nombre):
        self.apellido = p_apellido
        self.nombre = p_nombre

    def get_nombre(self):
        return self.nombre

    def get_apellido(self):
        return self.apellido
